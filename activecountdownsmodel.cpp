#include "activecountdownsmodel.h"

#include <QDebug>
#include <QVariant>

ActiveCountdownsModel::ActiveCountdownsModel(QObject * parent) : QAbstractListModel(parent) {
  QTimer * timer = new QTimer(this);
  connect(timer, &QTimer::timeout, this, [=]() {
    if (!m_countdownStarter->startedCountdowns().isEmpty()) {
      emit dataChanged(index(0), index(m_countdownStarter->startedCountdowns().size() - 1));
    }
  });
  timer->start(1000);
}

void ActiveCountdownsModel::cancel(int index) {
  // TODO ensure valid cancel
  m_countdownStarter->cancel(index);
}

int ActiveCountdownsModel::rowCount(const QModelIndex & parent) const {
  // For list models only the root node (an invalid parent) should return the
  // list's size. For all other (valid) parents, rowCount() should return 0 so
  // that it does not become a tree model.
  if (parent.isValid()) {
    return 0;
  }

  return m_countdownStarter->startedCountdowns().size();
}

QVariant ActiveCountdownsModel::data(const QModelIndex & index, int role) const {
  if (!index.isValid())
    return QVariant();

  StartedCountdown * c = m_countdownStarter->startedCountdowns().at(index.row());

  switch (role) {
  case TitleRole:
    return QVariant(c->title());
  case DurationRole:
    return QVariant(c->duration());
  case TimeRemainingRole:
    return QVariant(c->remainingTime());
  case ObjectRole:
    return qVariantFromValue(c);
  }

  return QVariant();
}

QHash<int, QByteArray> ActiveCountdownsModel::roleNames() const {
  QHash<int, QByteArray> names;
  names[TitleRole] = "title";
  names[MessageRole] = "message";
  names[DurationRole] = "duration";
  names[TimeRemainingRole] = "timeRemaining";
  names[ObjectRole] = "object";
  return names;
}

CountdownStarter * ActiveCountdownsModel::countdownStarter() const { return m_countdownStarter; }

void ActiveCountdownsModel::setCountdownStarter(CountdownStarter * countdownStarter) {

  beginResetModel();

  if (m_countdownStarter != nullptr) {
    m_countdownStarter->disconnect(this);
  }

  m_countdownStarter = countdownStarter;

  if (m_countdownStarter != nullptr) {
    connect(m_countdownStarter, &CountdownStarter::preCountdownStarted, this, [=]() {
      const int index = m_countdownStarter->startedCountdowns().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(m_countdownStarter, &CountdownStarter::postCountdownStarted, this, [=]() { endInsertRows(); });

    connect(m_countdownStarter, &CountdownStarter::preCountdownRemoved, this,
            [=](int index) { beginRemoveRows(QModelIndex(), index, index); });
    connect(m_countdownStarter, &CountdownStarter::postCountdownRemoved, this, [=]() { endRemoveRows(); });
  }

  endResetModel();
}
