#ifndef ACTIVECOUNTDOWNSMODEL_H
#define ACTIVECOUNTDOWNSMODEL_H

#include "countdownstarter.h"

#include <QAbstractListModel>

class ActiveCountdownsModel : public QAbstractListModel {
  Q_OBJECT
  Q_PROPERTY(CountdownStarter * countdownStarter READ countdownStarter WRITE setCountdownStarter)

  enum { TitleRole = Qt::UserRole, MessageRole, DurationRole, TimeRemainingRole, ObjectRole };

public:
  explicit ActiveCountdownsModel(QObject * parent = nullptr);

  Q_INVOKABLE void cancel(int index);

  // Basic functionality:
  int rowCount(const QModelIndex & parent = QModelIndex()) const override;

  QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

  QHash<int, QByteArray> roleNames() const override;

  CountdownStarter * countdownStarter() const;
  void setCountdownStarter(CountdownStarter * countdownStarter);

private:
  CountdownStarter * m_countdownStarter = nullptr;
};

#endif // ACTIVECOUNTDOWNSMODEL_H
