#include "configmodel.h"
#include "countdownconfig.h"

ConfigModel::ConfigModel(QObject * parent) : QAbstractListModel(parent) {}

int ConfigModel::rowCount(const QModelIndex & parent) const {
  // For list models only the root node (an invalid parent) should return the
  // list's size. For all other (valid) parents, rowCount() should return 0 so
  // that it does not become a tree model.
  if (parent.isValid() && mConfig != nullptr) {
    return 0;
  }

  return mConfig->countdowns().size();
}

QVariant ConfigModel::data(const QModelIndex & index, int role) const {
  if (!index.isValid() || mConfig == nullptr) {
    return QVariant();
  }

  const Countdown c = mConfig->countdowns().at(index.row());
  switch (role) {
  case TitleRole:
    return QVariant(c.title);
  case MessageRole:
    return QVariant(c.message);
  case DurationRole:
    return QVariant(c.duration);
  case ShowInTrayRole:
    return QVariant(c.showInTray);
  }
  return QVariant();
}

bool ConfigModel::setData(const QModelIndex & index, const QVariant & value,
                          int role) {
  if (mConfig == nullptr) {
    return false;
  }
  Countdown c = mConfig->countdowns().at(index.row());
  switch (role) {
  case TitleRole:
    c.title = value.toString();
    break;
  case MessageRole:
    c.message = value.toString();
    break;
  case DurationRole:
    c.duration = value.toInt();
    break;
  case ShowInTrayRole:
    c.showInTray = value.toBool();
    break;
  }
  if (mConfig->setItemAt(index.row(), c)) {
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
  }
  return false;
}

Qt::ItemFlags ConfigModel::flags(const QModelIndex & index) const {
  if (!index.isValid()) {
    return Qt::NoItemFlags;
  }

  return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ConfigModel::roleNames() const {
  QHash<int, QByteArray> names;
  names[TitleRole] = "title";
  names[MessageRole] = "message";
  names[DurationRole] = "duration";
  names[ShowInTrayRole] = "showInTray";
  return names;
}

void ConfigModel::addCountdown(QString title, int duration) {
	mConfig->addCountdown(title, duration);
}

void ConfigModel::removeCountdown(int index)
{
	mConfig->removeCountdownAtIndex(index);
}

CountdownConfig * ConfigModel::config() const { return mConfig; }

void ConfigModel::setConfig(CountdownConfig * value) {
  beginResetModel();

  if (mConfig != nullptr) {
    mConfig->disconnect(this);
  }

  mConfig = value;

  if (mConfig != nullptr) {
    connect(mConfig, &CountdownConfig::preCountdownAdded, this, [=]() {
      const int index = mConfig->countdowns().size();
      beginInsertRows(QModelIndex(), index, index);
    });
    connect(mConfig, &CountdownConfig::postCountdownAdded, this,
            [=]() { endInsertRows(); });

	connect(mConfig, &CountdownConfig::preCountdownRemoved, this, [=](int index) {
	  beginRemoveRows(QModelIndex(), index, index);
	});
	connect(mConfig, &CountdownConfig::postCountdownRemoved, this,
			[=]() { endRemoveRows(); });
  }

  endResetModel();
}
