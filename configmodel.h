#ifndef CONFIGMODEL_H
#define CONFIGMODEL_H

#include <QAbstractListModel>

class CountdownConfig;

class ConfigModel : public QAbstractListModel {
  Q_OBJECT
  Q_PROPERTY(CountdownConfig * config READ config WRITE setConfig)

public:
  explicit ConfigModel(QObject * parent = nullptr);

  enum { TitleRole = Qt::UserRole, MessageRole, DurationRole, ShowInTrayRole };

  // Basic functionality:
  int rowCount(const QModelIndex & parent = QModelIndex()) const override;

  QVariant data(const QModelIndex & index,
                int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex & index, const QVariant & value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex & index) const override;

  QHash<int, QByteArray> roleNames() const override;

  Q_INVOKABLE void addCountdown(QString title, int duration);
  Q_INVOKABLE void removeCountdown(int index);

  CountdownConfig * config() const;
  void setConfig(CountdownConfig * value);

private:
  CountdownConfig * mConfig = nullptr;
};

#endif // CONFIGMODEL_H
