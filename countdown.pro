#-------------------------------------------------
#
# Project created by QtCreator 2018-02-23T22:37:51
#
#-------------------------------------------------

QT       += core gui quick sql

TARGET = countdown
TEMPLATE = app

CONFIG += c++14
QMAKE_CXXFLAGS += -Werror

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


INCLUDEPATH += $$PWD/gsl

include(3rdparty/SortFilterProxyModel/SortFilterProxyModel.pri)

DEFINES += SOURCE_PATH=\"'\\"$${PWD}\\"'\"

SOURCES += \
    countdownconfig.cpp \
    configmodel.cpp \
    countdownstarter.cpp \
    main.cpp \
    countdownrepository.cpp \
    activecountdownsmodel.cpp \
    startedcountdown.cpp

HEADERS += \
    countdownconfig.h \
    configmodel.h \
    countdownstarter.h \
    countdownrepository.h \
    activecountdownsmodel.h \
    startedcountdown.h

FORMS +=

DISTFILES += \
    qml/NotificationDialog.qml \
    qml/Configuration.qml \
    qml/main.qml \
    qml/MainWindow.qml \
    TestMe.qml \
    qml/TestMeMore.qml \
    qml/Dashboard.qml

RESOURCES += \
    resources.qrc
