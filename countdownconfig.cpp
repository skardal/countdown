#include "countdownconfig.h"

#include <QVector>

CountdownConfig::CountdownConfig(std::unique_ptr<CountdownRepository> repo,
                                 QObject * parent)
    : QObject(parent), mRepo(std::move(repo)) {
  mCountdowns = QVector<Countdown>::fromStdVector(mRepo->FindAll());
}

QVector<Countdown> CountdownConfig::countdowns() const { return mCountdowns; }

bool CountdownConfig::setItemAt(int index, const Countdown & countdown) {
  if (index < 0 || index >= mCountdowns.size()) {
    return false;
  }

  const Countdown & old = mCountdowns.at(index);
  if (old.title == countdown.title && old.duration == countdown.duration &&
      old.message == countdown.message &&
      old.showInTray == countdown.showInTray) {
    return false;
  }

  mCountdowns[index] = countdown;
  return true;
}

void CountdownConfig::addCountdown(QString title, int duration) {
  emit preCountdownAdded();

  Countdown c = {title, "TODO: The message", duration, true};
  mCountdowns.append(c);

  emit postCountdownAdded();
}

void CountdownConfig::removeCountdownAtIndex(int index)
{
	emit preCountdownRemoved(index);

	mCountdowns.remove(index);

	emit postCountdownRemoved();
}

void CountdownConfig::saveToDisk() { mRepo->Save(mCountdowns.toStdVector()); }
