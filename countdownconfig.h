#ifndef COUNTDOWNCONFIG_H
#define COUNTDOWNCONFIG_H

#include <QObject>
#include <QVector>
#include <memory>

#include "countdownrepository.h"

struct Countdown {
  QString title;
  QString message;
  int duration;
  bool showInTray;
};

class CountdownConfig : public QObject {
  Q_OBJECT
public:
  explicit CountdownConfig(std::unique_ptr<CountdownRepository> repo,
                           QObject * parent = nullptr);

  QVector<Countdown> countdowns() const;

  bool setItemAt(int index, const Countdown & countdown);

signals:
  void preCountdownAdded();
  void postCountdownAdded();
  void preCountdownRemoved(int index);
  void postCountdownRemoved();

public slots:
  void addCountdown(QString title, int duration);
  void removeCountdownAtIndex(int index);
  void saveToDisk();

private:
  QVector<Countdown> mCountdowns;
  std::unique_ptr<CountdownRepository> mRepo;
};

#endif // COUNTDOWNCONFIG_H
