#include "countdownrepository.h"
#include "countdownconfig.h"

#include <QDebug>
#include <QtSql>

CountdownRepository::CountdownRepository() {
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("countdown.db");
  bool ok = db.open();
  qDebug() << (ok ? "Connected" : "Failed to connect");

  QSqlQuery query;
  query.exec("CREATE TABLE IF NOT EXISTS countdowns ("
             "id INTEGER, "
             "title TEXT, "
             "message TEXT, "
             "duration_in_seconds INTEGER, "
			 "show_in_tray INTEGER, "
             "PRIMARY KEY(id));");
}

std::vector<Countdown> CountdownRepository::FindAll() {
  QSqlDatabase db = QSqlDatabase::database();

  QSqlQuery findAll;
  findAll.exec("SELECT title, message, duration_in_seconds, show_in_tray FROM countdowns");

  std::vector<Countdown> result;
  while (findAll.next()) {
    result.push_back({findAll.value(0).toString(), findAll.value(1).toString(),
					  findAll.value(2).toInt(), findAll.value(3).toBool()});
  }

  return result;
}

void CountdownRepository::Save(std::vector<Countdown> countdowns) {
  QSqlDatabase db = QSqlDatabase::database();
  db.transaction();

  QSqlQuery deleteAll;
  deleteAll.exec("DELETE FROM countdowns");

  QSqlQuery batchInsert;
  batchInsert.prepare("INSERT INTO countdowns (title, message, "
					  "duration_in_seconds, show_in_tray) VALUES (?, ?, ?, ?)");

  QVariantList titles;
  QVariantList messages;
  QVariantList durations;
  QVariantList showInTrays;
  for (const auto & c : countdowns) {
    titles.append(QVariant(c.title));
    messages.append(QVariant(c.message));
    durations.append(QVariant(c.duration));
	showInTrays.append(QVariant(c.showInTray));
  }

  batchInsert.addBindValue(titles);
  batchInsert.addBindValue(messages);
  batchInsert.addBindValue(durations);
  batchInsert.addBindValue(showInTrays);
  batchInsert.execBatch();

  db.commit();
}
