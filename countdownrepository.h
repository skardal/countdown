#ifndef COUNTDOWNREPOSITORY_H
#define COUNTDOWNREPOSITORY_H

#include <vector>

struct Countdown;

class CountdownRepository {
public:
  explicit CountdownRepository();
  ~CountdownRepository() noexcept = default;

  CountdownRepository(const CountdownRepository & other) = delete;
  CountdownRepository(CountdownRepository && other) noexcept = default;

  std::vector<Countdown> FindAll();
  void Save(std::vector<Countdown> countdowns);
};

#endif // COUNTDOWNREPOSITORY_H
