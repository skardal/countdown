#include "countdownstarter.h"
#include <QTimer>

CountdownStarter::CountdownStarter(QObject * parent) : QObject(parent) {}

void CountdownStarter::start(const QString & title, const QString & message, int duration) {

  // TODO:
  // - add started to a model (so that it can be viewed)
  // - allow cancel
  // - started must contain (time of start, duration) – the goal is progress
  // bars

  QTimer * timer = new QTimer(this);
  auto x = new StartedCountdown(title, duration, timer, this);
  connect(timer, &QTimer::timeout, this, [=]() {
    emit countdownComplete(title, message, duration);
	remove(x);
  });
  timer->setSingleShot(true);
  timer->start(duration * 1000);

  emit preCountdownStarted();
  connect(x, &StartedCountdown::cancelled, this, [=]() { remove(x); });
  m_startedCountdowns.append(x);
  emit postCountdownStarted();

  emit countdownStarted(title, duration);
}

void CountdownStarter::cancel(int index) {
  auto countdown = m_startedCountdowns.at(index);

  if (countdown != nullptr) {
    emit preCountdownCancelled(index);
    countdown->timer()->stop();
    this->remove(countdown);
    emit postCountdownCancelled();
  }
}

void CountdownStarter::remove(StartedCountdown * countdown) {
  int index = m_startedCountdowns.indexOf(countdown);
  emit preCountdownRemoved(index);
  m_startedCountdowns.remove(index);
  emit postCountdownRemoved();
}

QVector<StartedCountdown *> CountdownStarter::startedCountdowns() const { return m_startedCountdowns; }
