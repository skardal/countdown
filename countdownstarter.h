#ifndef COUNTDOWNSTARTER_H
#define COUNTDOWNSTARTER_H

#include "startedcountdown.h"
#include <QObject>
#include <QTimer>
#include <QVector>

class CountdownStarter : public QObject {
  Q_OBJECT
public:
  explicit CountdownStarter(QObject * parent = nullptr);

  QVector<StartedCountdown *> startedCountdowns() const;

signals:
  void countdownComplete(QString title, QString message, int duration);
  void countdownStarted(QString title, int duration);

  void preCountdownStarted();
  void postCountdownStarted();
  void preCountdownCancelled(int index);
  void postCountdownCancelled();
  void preCountdownRemoved(int index);
  void postCountdownRemoved();

public slots:
  void start(const QString & title, const QString & message, int duration);
  void cancel(int index);

private:
  QVector<StartedCountdown *> m_startedCountdowns;
  void remove(StartedCountdown * countdown);
};

#endif // COUNTDOWNSTARTER_H
