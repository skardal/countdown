#include "activecountdownsmodel.h"
#include "configmodel.h"
#include "countdownconfig.h"
#include "countdownrepository.h"
#include "countdownstarter.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QDebug>

int main(int argc, char * argv[]) {
#if defined(Q_OS_WIN)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  QGuiApplication app(argc, argv);
  QGuiApplication::setQuitOnLastWindowClosed(false);

  qmlRegisterType<ConfigModel>("Config", 1, 0, "ConfigModel");
  qmlRegisterType<ActiveCountdownsModel>("Config", 1, 0, "ActiveCountdownsModel");
  qmlRegisterUncreatableType<CountdownConfig>("Config", 1, 0, "CountdownConfig",
                                              QStringLiteral("Not creatable from QML"));
  qmlRegisterUncreatableType<CountdownStarter>("Config", 1, 0, "CountdownStarter",
                                               QStringLiteral("Not creatable from QML"));

  std::unique_ptr<CountdownRepository> repo = std::make_unique<CountdownRepository>();
  CountdownConfig config(std::move(repo));
  CountdownStarter starter;

  QQmlApplicationEngine engine;

  engine.rootContext()->setContextProperty("myConfig", &config);
  engine.rootContext()->setContextProperty("starter", &starter);

  engine.load(QUrl::fromLocalFile(QStringLiteral(SOURCE_PATH "/qml/main.qml")));

  if (engine.rootObjects().isEmpty()) {
    return -1;
  }

  return app.exec();
}
