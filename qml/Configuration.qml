import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    Column {
        spacing: 5

        RowLayout {
            spacing: 5
            width: 600

            TextField {
                id: title
                placeholderText: "What happens then?"
            }

            TextField {
                id: seconds
                placeholderText: "Number of seconds"
            }

            Button {
                text: qsTr("Add")
                onClicked: {
                    choices.addCountdown(title.text, parseInt(seconds.text))
                    title.clear()
                    seconds.clear()
                }
                Layout.fillWidth: true
            }
        }       

        ListView {
            model: choices
            implicitWidth: 600
            implicitHeight: 300
            clip: true
            spacing: 5

            delegate: Component {
                ColumnLayout {
                    RowLayout {
                        spacing: 5
                        width: parent.width

                        TextField {
                            text: title
                            onEditingFinished: model.title = text
                        }

                        TextField {
                            text: duration
                            Layout.preferredWidth: 50
                            onEditingFinished: model.duration = text
                        }

                        CheckBox {
                            text: "Tray"
                            checked: showInTray
                            onClicked: model.showInTray = checked
                        }

                        Button {
                            text: qsTr("Start")
                            onClicked: {
                                starter.start(title, model.message, duration)
                            }                            
                        }

                        Button {
                            width: 20
                            text: "Details"
                            onClicked: msgRect.toggle()
                        }

                        Button {
                            text: "X"
                            Layout.preferredWidth: 50
                            onClicked: choices.removeCountdown(index)
                        }
                    }
                    Rectangle {
                        id: msgRect
                        Layout.fillWidth: true
                        height: 30
                        visible: true
                        border.width: 1
                        border.color: "gray"

                        onHeightChanged: {
                            visible = height !== 0
                        }

                        Behavior on height {
                            PropertyAnimation {
                                duration: 250
                            }
                        }

                        function toggle() {
                            height = visible ? 0 : 30
                            console.log("height is now " + height + " (visible? " + visible + ")")
                        }

                        TextArea {
                            id: message
                            anchors.fill: parent
                            text: model.message
                            onEditingFinished: model.message = text
                        }
                    }
                }
            }
        }

        Button {
            text: "Save"
            onClicked: myConfig.saveToDisk()
        }

        Loader {
            id: notificationLoader
        }

        Connections {
            target: notificationLoader.item
            onClosing: {
                notificationLoader.setSource("")
            }
            onRestartRequested: {
                notificationLoader.setSource("")
                starter.start(title, message, duration)
            }
        }

        Connections {
            target: starter
            onCountdownComplete: {
                //notificationLoader.setSource("NotificationDialog.qml", {"title" : title, "message": message, "duration": duration})
            }
        }
    }
}
