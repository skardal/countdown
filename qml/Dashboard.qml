import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Config 1.0

Item {    
    anchors.horizontalCenter: parent.horizontalCenter

    height: parent.height

    ActiveCountdownsModel {
        id: ongoingModel
        countdownStarter: starter
    }

    RowLayout {
        anchors.fill: parent

        ListView {
            model: choices
            spacing: 5
            delegate: Component {
                Button {
                    text: title
                    onClicked: {
                        starter.start(title, message, duration)
                    }
                }
            }
            Layout.fillHeight: true
            implicitWidth: contentItem.childrenRect.width
        }

        ListView {
            model: ongoingModel

            Layout.fillHeight: true
            Layout.fillWidth: true

            spacing: 5

            delegate: Component{
                RowLayout {
                    spacing: 5

                    Text {
                        text: title
                    }

                    ProgressBar {
                        from: 0
                        to: duration
                        value: duration - timeRemaining
                        width: parent.width

                        Behavior on value { PropertyAnimation {duration: 950} }
                        Layout.fillWidth: true
                    }

                    Button {
                        text: "Cancel"
                        anchors.right: parent.right
                        onClicked: {
                            object.cancel(index);
                        }
                    }
                }
            }
        }
    }
}
