import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQml 2.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

import Qt.labs.platform 1.0
import SortFilterProxyModel 0.2

import Config 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    x: (Screen.width - window.width) / 2
    y: (Screen.height - window.height) / 2
    title: qsTr("Configuration")

    header: TabBar {
        id: bar
        width: parent.width
        TabButton {
            text: qsTr("Dashboard")
        }
        TabButton {
            text: qsTr("Configuration")
        }
    }

    StackLayout {
        anchors.fill: parent
        anchors.margins: 5
        currentIndex: bar.currentIndex
        Dashboard {
            id: dashboardTab
        }
        Configuration {
            id: configTab
        }
    }

    ConfigModel {
        id: choices
        config: myConfig
    }   

    SystemTrayIcon {
        id: trayIcon
        visible: true
        iconSource: "qrc:/icons/remix.svg"

        menu: Menu {
            id: contextMenu
            Instantiator {
                id: ins
                model: SortFilterProxyModel{
                    sourceModel: choices
                    filters: ValueFilter {
                        roleName: "showInTray"
                        value: true
                    }
                }
                MenuItem {
                    text: title
                    onTriggered: starter.start(title, message, duration)
                }

                onObjectAdded: contextMenu.insertItem(index, object)
                onObjectRemoved: contextMenu.removeItem(object)
            }

            Connections {
                target: starter
                onCountdownComplete: {
                    trayIcon.showMessage("Countdown complete", message)
                }
            }

            MenuSeparator {}
            MenuItem {
                text: "Restore"
                onTriggered: {
                    window.show()
                    window.raise()
                    window.requestActivate()
                }
            }
            MenuItem {
                text: "Quit"
                shortcut: StandardKey.Quit
                onTriggered: Qt.quit()
            }
        }
    }
}
