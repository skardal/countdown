import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.3

Window {
    id: splash
    visible: true
    modality: Qt.ApplicationModal
    flags: Qt.SplashScreen
    width: 400
    height: 400

    property string title
    property string message
    property int duration

    signal restartRequested(string title, string message, int duration)

    Column {
        Rectangle {
            color: "lightskyblue"
            implicitWidth: 400
            implicitHeight: 100
            Text {
                text: title
                color: "navy"
                anchors.centerIn: parent
            }
        }

        Rectangle {
            implicitWidth: 400
            implicitHeight: 200
            Text {
                text: message
            }
        }

        Button {
            text: "Restart"
            implicitWidth: 400
            implicitHeight: 100
            onClicked: {
                splash.restartRequested(title, message, duration)
            }
        }
    }

    Timer {
        interval: 10000
        repeat: false
        running: true

        onTriggered: {
            splash.close()
        }
    }
}
