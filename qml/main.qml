import QtQuick 2.9

Loader {
    id: mainLoader
    source: "MainWindow.qml"
    active: true
    property int counter: 0

    onLoaded: {
        mainLoader.item.show()
    }

    Shortcut {
        sequence: "Ctrl+Shift+R"
        context: Qt.ApplicationShortcut
        onActivated: {
            var oldSource = mainLoader.source
            mainLoader.source = ""
            console.log(mainLoader.source)
            mainLoader.source = oldSource + "?" + counter++
            console.log(mainLoader.source)
        }
    }
}
