#include "startedcountdown.h"

StartedCountdown::StartedCountdown(const QString & title, int duration, QTimer * timer, QObject * parent)
    : QObject(parent), m_title(title), m_duration(duration), m_timer(timer) {}

int StartedCountdown::remainingTime() const { return m_timer->remainingTime() / 1000; }

QTimer * StartedCountdown::timer() const { return m_timer; }

int StartedCountdown::duration() const { return m_duration; }

QString StartedCountdown::title() const { return m_title; }

void StartedCountdown::cancel() {
  m_timer->stop();
  emit cancelled();
}
