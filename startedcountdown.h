#ifndef STARTEDCOUNTDOWN_H
#define STARTEDCOUNTDOWN_H

#include <QObject>
#include <QTimer>

class StartedCountdown : public QObject {
  Q_OBJECT

  Q_PROPERTY(int remainingTime READ remainingTime NOTIFY remainingTimeChanged)

public:
  explicit StartedCountdown(const QString & title, int duration, QTimer * timer, QObject * parent = nullptr);

  int remainingTime() const;
  QTimer * timer() const;

  int duration() const;

  QString title() const;

signals:
  void remainingTimeChanged(int remainingTime);
  void cancelled();

public slots:
  void cancel();

private:
  const QString m_title;
  const int m_duration;
  QTimer * m_timer;
};

typedef StartedCountdown * StartedCountdownStar;
Q_DECLARE_METATYPE(StartedCountdownStar);

#endif // STARTEDCOUNTDOWN_H
